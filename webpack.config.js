const path = require('path');

const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
  entry: './src/backend.ts',
  mode: 'development',
  devtool: 'inline-source-map',
  target: 'node',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'backend.js'
  },
  resolve: {
    extensions: [ '.ts', '.js' ],
    modules: [
      path.resolve(__dirname, 'node_modules')
    ]
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'ts-loader',
            options: {
              transpileOnly: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin()
  ]
};
