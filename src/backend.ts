import express from 'express';
import http from 'http';
import bodyParser from 'body-parser';
import crypto from 'crypto';
import WebSocket from 'ws';

import * as config  from '../config.json';

import {SessionManager} from './classes/SessionManager';
import {RoomApiHandler} from './classes/RoomApiHandler';
import {SignalingMessageHelper} from './classes/SignalingMessageHelper';
import {BackendApiRequestHelper} from './classes/BackendApiRequestHelper';
import {JanusWebRtcServerHelper} from './classes/WebRtcServerHelpers/JanusWebRtcServerHelper';

let webRtcServerHelper;
if(process.env.USE_JANUS || config.janusClient) {
  webRtcServerHelper = new JanusWebRtcServerHelper({
    url: process.env.JANUS_URL || config.janusClient.url,
    apiSecret: process.env.JANUS_API_SECRET || config.janusClient.apiSecret
  });
}

const backendApiRequestHelper = new BackendApiRequestHelper();

const app = express();

app.use(bodyParser.text({ type: 'application/json' }));
app.use((request, response, next) => {
  const randomString = request.headers['spreed-signaling-random'];
  const checksum = request.headers['spreed-signaling-checksum'];

  if(!randomString) {
    return response.status(401).send();
  }

  const hmac = crypto.createHmac('sha256', process.env.SHARED_SECRET || config.sharedSecret);
  hmac.update(randomString.toString());
  hmac.update(request.body);
  const newChecksum = hmac.digest('hex');

  if(checksum === newChecksum) {
    request.body = JSON.parse(request.body);
    next();
  } else {
    response.status(401).send();
  }
});

const server = http.createServer(app);

const webSocketServer = new WebSocket.Server({
  server: server,
  path: '/spreed'
});

const roomApiHandler = new RoomApiHandler();
app.use(roomApiHandler.getRouter());

const sessionManager = new SessionManager({ roomApiHandler, webRtcServerHelper });

new SignalingMessageHelper(
  webSocketServer,
  sessionManager,
  backendApiRequestHelper,
  webRtcServerHelper
);

server.listen(process.env.BACKEND_PORT || config.port, () => { console.log('server started'); });
