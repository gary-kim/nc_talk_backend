import {Janus, Session as JanusSession, VideoRoomPluginListenerHandle, VideoRoomPluginPublisherHandle} from  'janus-videoroom-client';

export class JanusWebRtcServerHelper implements AbstractWebRtcServerHelper {

  private janusClient: Janus;

  private sessions: Map<number, Session> = new Map();

  constructor(options: JanusOptions) {
    this.janusClient = new Janus(options);

    this.janusClient.onConnected(this.onConnectedHandler.bind(this));
    this.janusClient.onDisconnected(this.onDisconnectedHandler.bind(this));
    this.janusClient.onError(this.onErrorHandler.bind(this));

    this.janusClient.onEvent(this.onEventHandler.bind(this));

    this.connect();
  }

  private onConnectedHandler() {
    console.log('Janus: connected');
  }

  private onDisconnectedHandler() {
    console.log('Janus: disconnected, reconnecting...');
    this.connect();
  }

  private onErrorHandler() {
    throw new Error('Janus: could not connect!');
  }

  private onEventHandler(event) {
    console.log('JANUS EVENT:', event);
  }

  private connect() {
    this.janusClient.connect();
  }

  async createSession(): Promise<string> {
    const session = await this.janusClient.createSession();
    this.sessions.set(session.getId(), {
      janusSession: session,
      publisherHandle: null,
      listenerHandles: new Map()
    });
    return session.getId().toFixed();
  }

  async destroySession(sessionId: string): Promise<void> {
    return await this.janusClient.destroySession(Number(sessionId));
  }

  async getRoomInfos(sessionId: string) {
    const defaultHandle = await this.getDefaultHandle(sessionId);
    const roomList = await defaultHandle.list();
    return roomList.list;
  }

  private async getRoomInfo(sessionId: string, roomId: string) {
    const roomInfos = await this.getRoomInfos(sessionId);
    return roomInfos.find(roomInfo => roomInfo.description === roomId);
  }

  async createRoom(sessionId: string, roomId: string) {
    const defaultHandle = await this.getDefaultHandle(sessionId);
    const room = await defaultHandle.create({
      description: roomId,
      is_private: false,
      publishers: 100
    });
    console.log('JANUS: room created', roomId);
    return room.room;
  }

  async getOrCreateRoom(sessionId: string, roomId: string) {
    const roomList = await this.getRoomInfos(sessionId);
    const room = roomList.find(roomInfo => roomInfo.description === roomId);
    if(!room) {
      return await this.createRoom(sessionId, roomId);
    } else {
      return room;
    }
  }

  async joinRoom(sessionId: string, roomId: string) {
    const room = await this.getOrCreateRoom(sessionId, roomId);
  }

  async leaveRoom(sessionId: string, roomId: string) {

  }

  async deleteRoom(sessionId: string, roomId: string) {
    const roomInfo = this.getRoomInfo(sessionId, roomId);

    const defaultHandle = await this.getDefaultHandle(sessionId);
    defaultHandle.destroy({ room: roomInfo.room });
  }

  async publishInRoom(sessionId: string, roomId: string, offerSdp: string) {
    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(sessionId, roomId);
    if(!room) throw new RoomError(roomId);

    const publisherHandle = await session.janusSession.videoRoom().publishFeed(room.room, offerSdp);
    session.publisherHandle = publisherHandle;

    return publisherHandle.getAnswer();
  }

  async getFeedsOfRoom(sessionId: string, roomId: string) {
    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(sessionId, roomId);
    if(!room) throw new RoomError(roomId);

    return await session.janusSession.videoRoom().getFeeds(room.room);
  }

  async subscribeToFeedOfRoom(sessionId: string, roomId: string, sessionIdToSubscribe: string) {
    const session = this.getSession(sessionId);

    const room = await this.getRoomInfo(sessionId, roomId);
    if(!room) throw new RoomError(roomId);

    const feedId = this.getFeedIdFromSessionId(sessionIdToSubscribe);
    if(!feedId) throw new Error('feed does not exist');

    const listenerHandle = await session.janusSession.videoRoom().listenFeed(room.room, feedId);
    session.listenerHandles.set(feedId, listenerHandle);

    return listenerHandle.getOffer();
  }

  async setAnswerToSubscription(sessionId: string, sessionIdToSubscribe: string, answerSdp: string) {
    const session = this.getSession(sessionId);
    const feedId = this.getFeedIdFromSessionId(sessionIdToSubscribe);
    if(!feedId) throw new Error('feed does not exist!');

    const listenerHandle = session.listenerHandles.get(feedId);
    if(!listenerHandle) throw new Error('not subscribed to feed!');

    await listenerHandle.setRemoteAnswer(answerSdp);
  }

  async unpublish(sessionId: string) {
    const session = this.getSession(sessionId);
    if(session.publisherHandle) {
      session.publisherHandle.unpublish();
    }
  }

  async unsubscribe(sessionId: string, sessionIdToUnsubscribe: string) {
    const session = this.getSession(sessionId);
    const feedId = this.getFeedIdFromSessionId(sessionIdToUnsubscribe);
    if(!feedId) throw new Error('feed does not exist');

    const listenerHandle = session.listenerHandles.get(feedId);
    listenerHandle?.leave();
  }

  async trickleCandidate(sessionId: string, candidate: Candidate) {
    const handle = await this.getDefaultHandle(sessionId);
    handle.trickle(candidate);
  }

  async trickleCandidates(sessionId: string, candidates: Array<Candidate>) {
    const handle = await this.getDefaultHandle(sessionId);
    handle.trickles(candidates);
  }

  async trickleCompleted(sessionId: string) {
    const handle = await this.getDefaultHandle(sessionId);
    handle.trickleCompleted();
  }

  private getSession(sessionId: string): Session {
    const session = this.sessions.get(Number(sessionId));
    if(!session) throw new SessionError();

    return session;
  }

  private async getDefaultHandle(sessionId: string) {
    const session = this.getSession(sessionId);
    return await session.janusSession.videoRoom().defaultHandle();
  }

  private getFeedIdFromSessionId(sessionId: string): number|undefined {
    const session = this.getSession(sessionId);
    return session.publisherHandle?.getPublisherId();
  }

}

interface Session {
  janusSession: JanusSession;
  publisherHandle: VideoRoomPluginPublisherHandle|null;
  listenerHandles: Map<number, VideoRoomPluginListenerHandle>
}

interface JanusOptions {
  url: string,
  apiSecret: string|null
}

class SessionError extends Error {
  // TODO
}

class RoomError extends Error {

  private _roomId: string

  constructor(roomId: string) {
    super();
    this.message = 'Room does not exist!';
    this._roomId = roomId;
  }

  getRoomId() {
    return this._roomId;
  }

}
