declare module 'janus-videoroom-client' {

  export class Janus {

    constructor(options?: ClientOptions);

    getVersion(): string;

    isConnected(): boolean;
    isConnecting(): boolean;
    isClosing(): boolean;

    connect(): void;
    disconnect(): void;

    open(): void;
    close(options): void;

    message(message): void;

    error(err): void;

    getConnectionState(): 'connected'|'disconnected';

    setConnectionTimeout(timeout): void;
    startConnectionTimeout(): void;
    stopConnectionTimeout(): void;

    dispatchObject(obj): void;

    delegateEvent(event): void;

    sendObject(obj): Promise;

    createTransaction(options): Transaction;

    request(req, options): Promise;

    hasSession(id: number): boolean;
    addSession(session: Session): void;
    deleteSession(id: number): void;
    createSession(): Promise<Session>;
    claimSession(sessionId: number): Promise<Session>;
    destroySession(id: number): Promise<void>;

    getInfo(): Promise<ServerInfo>;

    onConnected(listener: () => void): void;
    onDisconnected(listener: () => void): void;
    onError(listener: (err) => void): void;
    onEvent(listener: (event) => void): void;

  }

  interface ClientOptions {
    url?: string
    logger?
    requestTimeout?: number
    WebSocket?
    connectionTimeout?: number
    reconnect?: boolean
    token?: string|null
    apiSecret?: string|null
    handshakeTimeout?: number|undefined
  }

  export class Session {

    constructor(id: number, janus: Janus);

    keepAlive(); // TODO return value
    startKeepAlive(): void;
    stopKeepAlive(): void;

    request(obj, options); // TODO return value

    createPluginHandle(plugin, options): Promise<number>;

    getId(): number;
    getState(): 'alive'|'dying'|'dead';

    isAlive(): boolean;

    timout(): void;

    onTimeout(listener: () => void): void;
    onKeepAlive(listener: (keepalive: boolean) => void): void;
    onError(listener: (err) => void): void;
    onEvent(listener: (event) => void): void;

    event(event): void;

    destroy(): Promise<void>;

    videoRoom(): VideoRoomPlugin;

  }

  class Plugin {

    constructor(options);

    getName(): string;
    getFullName(): string;

    getSession(): Session;
    setSession(session: Session): void;

    addHandle(handle: PluginHandle): void;
    hasHandle(id: number): boolean;
    getHandle(id: number): PluginHandle;
    removeHandle(id: number): void;
    createHandle(options): Promise<number>;
    destroyHandle(handle: PluginHandle): Promise<void>;
    destroyHandleById(id: number): Promise<void>

  }

  class VideoRoomPlugin extends Plugin {

    constructor(options);

    defaultHandle(): Promise<VideoRoomPluginHandle>;
    createVideoRoomHandle(options): Promise<VideoRoomPluginHandle>;
    attachVideoRoomHandle(handleId: number, opaqueId?: string): Promise<VideoRoomPluginHandle>;

    createPublisherHandle(room: number, opaqueId?: string): Promise<VideoRoomPluginPublisherHandle>;
    attachPublisherHandle(handleId: number, room: number, opqueId?: string): Promise<VideoRoomPluginPublisherHandle>;

    createListenerHandle(room: number, feed: number, opaqueId?: string): Promise<VideoRoomPluginListenerHandle>;
    attachListenerHandle(handleId: number, room: number, feed: number, opaqueId?: string): Promise<VideoRoomPluginListenerHandle>;

    publishFeed(room: number, offer, opaqueId?: string): Promise<VideoRoomPluginPublisherHandle>;
    listenFeed(room: number, feed: number , opaqueId?: string): Promise<VideoRoomPluginListenerHandle>;

    getFeeds(room: number): Promise<Array<number>>;
    getFeedsExclude(room: number, feed: number): Promise<Array<number>>;

  }

  class PluginHandle {

    constructor(options);

    getId(): number;
    getSession(): Session;
    getPlugin(): VideoRoomPlugin;

    isConnected(): 'connected'|'disconnected';
    isDisposed(): boolean;

    detach():;
    hangup(): Promise<Result>;
    trickle(candidate: Candidate):;
    trickles(candidates: Array<Candidate>):;
    trickleCompleted():;

    event(event): void;

    onWebrtcUp(listener): void;
    onMedia(listener): void;
    onHangup(listener): void;
    onSlowLink(listener): void;
    onDetached(listener): void;
    onEvent(listener): void;
    onTrickle(listener): void;

    request(obj, options):;
    requestMessage(body, options): Promise<PluginResponse>;

    dispose(): Promise<void>;

  }

  class VideoRoomPluginHandle extends PluginHandle {

    constructor(options);

    create(options): Promise<{ room, response: PluginResponse }>;
    destroy(options): Promise<{ response: PluginResponse }>;
    exists(options): Promise<{ exists: boolean, response: PluginResponse }>;

    list(): Promise<{ list: Array<RoomInfo>, response: PluginResponse }>;
    listParticipants(options): Promise<{ participants, response: PluginResponse }>;

    join(options): Promise<JoinPluginResponse>;
    joinPublisher(options): Promise<JoinPluginResponse>;
    joinListener(options): Promise<JoinPluginResponse>;

    configure(options): Promise<{ response: PluginResponse }>;

    joinAndConfigure(options): Promise<{ id, jsep, publishers, response: PluginResponse }>;

    publish(options): Promise<{ response: PluginResponse }>;
    unpublish(options?): Promise<{ response: PluginResponse }>;

    start(options): Promise<{ response: PluginResponse }>;
    pause(options): Promise<{ response: PluginResponse }>;

    switch(options): Promise<{ response: PluginResponse }>;

    stop(options): Promise<{ response: PluginResponse }>;

    add(options): Promise<>;
    remove(options): Promise<>;

    leave(options?): Promise<>;

    publishFeed(options): Promise<>;

    listenFeed(options): Promise<>;

  }

  interface JoinPluginResponse {
    id,
    jsep,
    response: PluginResponse
  }

  class VideoRoomPluginPublisherHandle extends VideoRoomPluginHandle {

    constructor(options);

    getPublisherId(): number;
    getRoom(): number;
    getAnswer(): answer;

    createAnswer(offer): Promise<void>

  }

  class VideoRoomPluginListenerHandle extends VideoRoomPluginHandle {

    constructor(options);

    getRoom(): number;
    getFeed(): feed;
    getOffer(): offer;

    createOffer(): Promise<void>;

    setRemoteAnswer(answer): Promise<void>;

  }

  class ClientResponse {

    constructor(req, res);

    getRequest(): request;
    getResponse(): response;

    getType(): type|null;
    getJsep(): jsep|null;

    isError(): boolean;
    isAck(): boolean;
    isSuccess(): boolean;

  }

  class PluginResponse extends ClientResponse {

    constructor(req, res);

    isError(): boolean;

    getName(): string|null;
    getData(): data|null;

  }

  interface RoomInfo {
    room: number;
    description: string;
    pin_required: boolean;
    max_publishers: number;
    // TODO...
  }

}
