import WebSocket from 'ws';
import {Session, MessageRecipientData} from './Session';
import {BackendApiRequestHelper} from './BackendApiRequestHelper';
import {RoomApiHandler, RoomApiRequestDataProperties, Participant} from './RoomApiHandler';

export class SessionManager {

  private sessions: Array<Session>

  private webRtcServerHelper?: AbstractWebRtcServerHelper;
  private roomApiHandler: RoomApiHandler;

  constructor({ webRtcServerHelper, roomApiHandler }: ConstructorServices) {
    this.sessions = [];
    this.webRtcServerHelper = webRtcServerHelper;
    this.roomApiHandler = roomApiHandler;

    this.roomApiHandler.on('roomListEvent', this.onRoomListEvent.bind(this));
    this.roomApiHandler.on('participantsListEvent', this.onParticipantsListEvent.bind(this));
    this.roomApiHandler.on('roomMessage', this.onRoomMessage.bind(this));
  }

  getSessionById(sessionId: string): Session|null {
    return this.sessions.find(session => session.getId() === sessionId) || null;
  }

  getSessionsByIds(sessionIds: Array<string>): Array<Session> {
    return this.sessions.filter(session => sessionIds.includes(session.getId()));
  }

  getSessionByNcId(ncSessionId: string): Session|null {
    return this.sessions.find(session => session.getNcSessionId() === ncSessionId) || null;
  }

  getSessionsForRoomId(roomId: string): Array<Session> {
    return this.sessions.filter(session => session.getRoomId() === roomId);
  }

  getSessionsForUserId(userId: string): Array<Session> {
    return this.sessions.filter(session => session.getUserId() === userId);
  }

  getSessionsForUserIds(userIds: Array<string>): Array<Session> {
    return this.sessions.filter(session => userIds.includes(session.getUserId() || ''));
  }

  async createSession(sessionCreationData: SessionCreationData) {
    const newSession = new Session({
      sessionId: await this.getNewSessionId(),
      socket: sessionCreationData.socket
    }, {
      backendApiRequestHelper: sessionCreationData.backendApiRequestHelper,
      webRtcServerHelper: sessionCreationData.webRtcServerHelper
    });

    if(sessionCreationData.ncUserId) {
      newSession.setUserId(sessionCreationData.ncUserId);
    }

    newSession.on('socketClosed', () => {
      this.deleteSession(newSession);
    });

    newSession.on('roomEvent', this.onRoomEvent.bind(this));

    newSession.on('sessionMessage', this.onSessionMessage.bind(this));
    newSession.on('userMessage', this.onUserMessage.bind(this));
    newSession.on('roomMessage', this.onRoomMessage.bind(this));

    this.sessions.push(newSession);
    return newSession;
  }

  private async getNewSessionId(): Promise<string> {
    if(this.webRtcServerHelper) {
      return this.webRtcServerHelper.createSession();
    } else {
      return Date.now().toFixed();
    }
  }

  deleteSession(session: Session) {
    this.deleteSessionById(session.getId());
  }

  deleteSessionById(sessionId: string) {
    const index = this.sessions.findIndex(session => session.getId() === sessionId);
    if(index >= 0) {

      const sessionToDelete = this.sessions[index];
      sessionToDelete.destructor();

      this.sessions.splice(index, 1);
    }
  }

  private onRoomEvent({ roomId, type, eventData }: { roomId: string, type: RoomEventType, eventData: RoomEventData }) {
    const sendData = {
      target: 'room',
      type: type,
      [type]: eventData
    };
    const roomSessions = this.getSessionsForRoomId(roomId);
    roomSessions.forEach((session) => {
      this.sendEventMessage(session, sendData);
    });
  }

  private onRoomListEvent({ type, userIds, sessionIds, eventData }: { type: RoomListEventType, userIds?: Array<string>, sessionIds?: Array<string>, eventData: RoomListEventData }) {
    const sendData = {
      target: 'roomlist',
      type: type,
      [type]: eventData
    };
    const sessions = userIds ? this.getSessionsForUserIds(userIds) : this.getSessionsByIds(sessionIds);
    sessions.forEach(session => {
      this.sendEventMessage(session, sendData);
    });
  }

  private onParticipantsListEvent({ sessionIds, roomId, eventData }: { sessionIds?: Array<string>, roomId?: string, eventData: ParticipantsListEventData }) {
    const changedParticipants = eventData.users.map(item => {
      const session = this.getSessionByNcId(item.sessionId);
      if(!session) return item;
      const sessionId = session.getId();
      return {
        ...item,
        sessionId: sessionId
      };
    });

    const sendData = {
      target: 'participants',
      type: 'update',
      update: {
        ...eventData,
        users: changedParticipants
      }
    };
    const sessions = sessionIds ? this.getSessionsByIds(sessionIds) : this.getSessionsForRoomId(roomId);
    sessions.forEach(session => {
      this.sendEventMessage(session, sendData);
    });

    changedParticipants.forEach(participant => {
      if(!participant.inCall) this.webRtcServerHelper?.unpublish(participant.sessionId);
    });
  }

  private sendEventMessage(session: Session, eventData: MessageEventData) {
    const sendData = {
      type: 'event',
      event: eventData
    };
    session.sendSocketMessage(sendData);
  }

  private async onSessionMessage({ recipient, senderSessionId, data }: { recipient: MessageRecipientData }) {
    console.log('SESSION MESSAGE:', data);

    if(recipient.type !== 'session') return; // TODO check better?

    const session = this.getSessionById(recipient.sessionid);
    if(!session) return; // TODO Error handling?

    const senderSession = this.getSessionById(senderSessionId);

    const payload = data.payload;

    switch(data.type) {
      case 'offer':
        senderSession.sendMessageMessageResponse(recipient, {
          type: 'answer',
          roomType: data.roomType,
          payload: {
            type: 'answer',
            sdp: await this.handleOffer(session, payload),
            nick: payload.nick
          }
        });

      case 'candidate':
        await this.handleCandidate(session, payload.candidate);
        break;

      case 'requestoffer':
        senderSession.sendMessageMessageResponse(recipient, {
          type: 'offer',
          from: data.to,
          roomType: 'video',
          payload: {
            type: 'offer',
            sdp: await this.handleOfferRequest(session, senderSessionId),
            // nick: payload.nick
          }
        });

      case 'answer':
        await this.handleAnswer(session, senderSessionId, payload);
        break;

      default:
        console.warn('Type of session message not understood!', data);
    }
    // if(session) this._sendSocketMessage(session.socket, data);
  }

  private async handleOffer(session: Session, payload) {
    if(this.webRtcServerHelper) {
      return this.webRtcServerHelper.publishInRoom(session.getId(), session.getRoomId(), payload.sdp);
    } else {

    }
  }

  private async handleCandidate(session: Session, candidate) {
    if(this.webRtcServerHelper) {
      return await this.webRtcServerHelper.trickleCandidate(session.getId(), candidate);
    } else {

    }
  }

  private async handleOfferRequest(session: Session, senderSessionId: string) {
    if(this.webRtcServerHelper) {
      session.subscriberIds.set(senderSessionId, true);
      return await this.webRtcServerHelper.subscribeToFeedOfRoom(senderSessionId, session.getRoomId(), session.id);
    } else {

    }
  }

  private async handleAnswer(session: Session, senderSessionId: string, payload) {
    if(this.webRtcServerHelper) {
      await this.webRtcServerHelper.setAnswerToSubscription(senderSessionId, session.getId(), payload.sdp);
    } else {

    }
  }

  private onUserMessage({ userId, data }) {
    const sessions = this.getSessionsForUserId(userId);
    sessions.forEach((session) => {
      session.sendSocketMessage(data);
    });
  }

  private onRoomMessage({ roomId, data }) {
    const sessions = this.getSessionsForRoomId(roomId);
    sessions.forEach((session) => {
      session.sendSocketMessage(data);
    });
  }

  private sendSocketMessage(socket: WebSocket, data: Object) {
    socket.send(JSON.stringify(data));
  }

}

interface ConstructorServices {
  webRtcServerHelper?: AbstractWebRtcServerHelper;
  roomApiHandler: RoomApiHandler;
}

interface SessionCreationData {
  ncUserId?: string;
  socket: WebSocket;
  backendApiRequestHelper: BackendApiRequestHelper;
  webRtcServerHelper?: AbstractWebRtcServerHelper;
}

interface RoomEventData {

}

interface RoomListEventData {
  roomid: string;
  properties?: RoomApiRequestDataProperties; // not available in 'disinvite' message
}

interface ParticipantsListEventData {
  roomid: string,
  users: Array<Participant>
}

interface MessageEventData {

}

enum RoomEventType {
  JOIN = 'join',
  LEAVE = 'leave',
  CHANGE = 'change',
  MESSAGE = 'message'
}

export enum RoomListEventType {
  INVITE = 'invite',
  DISINVITE = 'disinvite',
  UPDATE = 'update'
}
