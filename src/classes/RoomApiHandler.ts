import {Router, Response, Request} from 'express';
import {RoomListEventType} from './SessionManager';

import express from 'express';

export class RoomApiHandler {

  private router: Router;

  private eventHandlers: Map<string, Array<EventHandler>> = new Map();

  constructor() {
    this.router = express.Router();
    this.setUpRoutes();
  }

  getRouter(): Router {
    return this.router;
  }

  private setUpRoutes() {
    this.router.post('/api/v1/room/:roomId', this.handleRoomApiRequest.bind(this));
  }

  private handleRoomApiRequest(request: Request, response: Response) {
    const roomId = request.params.roomId;

    const body: RoomApiRequestBody = request.body;

    switch(body.type) {
      case RoomApiRequestType.INVITE:
        this.handleInviteRequest(roomId, body.invite);
        break;

      case RoomApiRequestType.DISINVITE:
        this.handleDisinviteRequest(roomId, body.disinvite);
        break;

      case RoomApiRequestType.UPDATE:
        this.handleUpdateRequest(roomId, body.update);
        break;

      case RoomApiRequestType.DELETE:
        this.handleDeleteRequest(roomId, body.delete);
        break;

      case RoomApiRequestType.PARTICIPANTS:
        this.handleParticipantsRequest(roomId, body.participants);
        break;

      case RoomApiRequestType.INCALL:
        this.handleIncallRequest(roomId, body.incall);
        break;

      case RoomApiRequestType.MESSAGE:
        this.handleMessageRequest(roomId, body.message);
        break;

      default:
        console.warn('API REQ BODY:', request.body);
        // TODO
        break;
    }

    response.status(200).send();
  }

  // Room::EVENT_AFTER_USERS_ADD
  private handleInviteRequest(roomId: string, data: RoomApiInviteRequestData) {
    console.log('API INVITE:', data);
    const sendData = {
      roomid: roomId,
      properties: data.properties
    };
    this.fireEvent('roomListEvent', { type: RoomListEventType.INVITE, userIds: data.userids, eventData: sendData });
  }

  // Room::EVENT_AFTER_USER_REMOVE
  // Room::EVENT_AFTER_PARTICIPANT_REMOVE -> sessions
  private handleDisinviteRequest(roomId: string, data: RoomApiDisinviteRequestData) {
    console.log('API DISINVITE:', data);
    const sendData = {
      roomid: roomId,
      properties: data.properties
    };
    this.fireEvent('roomListEvent', { type: RoomListEventType.DISINVITE, userIds: data.userids, sessionIds: data.sessionids, eventData: sendData });
  }

  // Room::EVENT_AFTER_NAME_SET
  // Room::EVENT_AFTER_PASSWORD_SET
  // Room::EVENT_AFTER_TYPE_SET
  // Room::EVENT_AFTER_READONLY_SET
  // Room::EVENT_AFTER_LOBBY_STATE_SET
  // TODO remove handler with "roomModified" in favour of handler with
  // "participantsModified" once the clients no longer expect a
  // "roomModified" message for participant type changes.
  // Room::EVENT_AFTER_PARTICIPANT_TYPE_SET
  private handleUpdateRequest(roomId: string, data: RoomApiUpdateRequestData) {
    console.log('API UPDATE:', data);
    const sendData = {
      roomid: roomId,
      properties: data.properties
    };
    this.fireEvent('roomListEvent', { type: RoomListEventType.UPDATE, userIds: data.userids, eventData: sendData });
  }

  // Room::EVENT_BEFORE_ROOM_DELETE
  private handleDeleteRequest(roomId: string, data: RoomApiDeleteRequestData) {
    console.log('API DELETE:', data);
    // TODO
    const sendData = {
      roomid: roomId
    };

    this.fireEvent('roomListEvent', { type: RoomListEventType.DISINVITE, userIds: data.userids, eventData: sendData });
  }

  // Room::EVENT_AFTER_PARTICIPANT_TYPE_SET
  // Room::EVENT_AFTER_GUESTS_CLEAN
  // GuestManager::EVENT_AFTER_NAME_UPDATE
  private handleParticipantsRequest(roomId: string, data: RoomApiParticipantsRequestData) {
    console.log('API PARTICIPANTS:', data);
    const sendData = {
      roomid: roomId,
      users: data.changed
    };
    const sessionIds = data.users.map(user => user.sessionId);
    this.fireEvent('participantsListEvent', { sessionIds: sessionIds, eventData: sendData });
  }

  // Room::EVENT_AFTER_SESSION_JOIN_CALL
  // Room::EVENT_AFTER_SESSION_LEAVE_CALL
  private handleIncallRequest(roomId: string, data: RoomApiIncallRequestData) {
    console.log('API INCALL:', data);
    const sendData = {
      roomid: roomId,
      users: data.changed
    };
    this.fireEvent('participantsListEvent', { roomId: roomId, eventData: sendData });
  }

  // ChatManager::EVENT_AFTER_MESSAGE_SEND
  // ChatManager::EVENT_AFTER_SYSTEM_MESSAGE_SEND
  private handleMessageRequest(roomId: string, data: RoomApiMessageRequestData) {
    console.log('API MESSAGE:', data);
    if(data.data && data.data.chat) {
      this.fireEvent('roomMessage', { roomId: roomId, data: data.data });
    }
  }

  on(eventName: string, eventHandler: EventHandler) {
    const eventHandlers = this.eventHandlers.get(eventName);
    if(!eventHandlers) {
      this.eventHandlers.set(eventName, [ eventHandler ]);
    } else {
      eventHandlers.push(eventHandler);
    }
  }

  fireEvent(eventName: string, eventData) {
    const eventHandlers = this.eventHandlers.get(eventName);
    eventHandlers?.forEach(handler => {
      handler(eventData);
    });
  }

}

interface EventHandler {
  (data): void;
}

// export namespace RoomApiHandler {

type RoomApiRequestBody =
  RoomApiInviteRequestBody |
  RoomApiDisinviteRequestBody |
  RoomApiUpdateRequestBody |
  RoomApiDeleteRequestBody |
  RoomApiParticipantsRequestBody |
  RoomApiIncallRequestBody |
  RoomApiMessageRequestBody;

interface RoomApiInviteRequestBody {
  type: RoomApiRequestType.INVITE;
  invite: RoomApiInviteRequestData
}

interface RoomApiInviteRequestData {
  userids: Array<string>;
  alluserids: Array<string>;
  properties: RoomApiRequestDataProperties;
}

interface RoomApiDisinviteRequestBody {
  type: RoomApiRequestType.DISINVITE;
  disinvite: RoomApiDisinviteRequestData;
}

type RoomApiDisinviteRequestData = RoomApiDisinviteUserIdsRequestData | RoomApiDisinviteSessionIdsRequestData;

interface RoomApiDisinviteRequestDataCommon {
  alluserids: Array<string>;
  properties: RoomApiRequestDataProperties;
}

type RoomApiDisinviteUserIdsRequestData = RoomApiDisinviteRequestDataCommon & {
  userids: Array<string>;
}

type RoomApiDisinviteSessionIdsRequestData = RoomApiDisinviteRequestDataCommon & {
  sessionids: Array<string>;
}

interface RoomApiUpdateRequestBody {
  type: RoomApiRequestType.UPDATE;
  update: RoomApiUpdateRequestData;
}

interface RoomApiUpdateRequestData {
  userids: Array<string>;
  properties: RoomApiRequestDataProperties;
}

interface RoomApiDeleteRequestBody {
  type: RoomApiRequestType.DELETE;
  delete: RoomApiDeleteRequestData;
}

interface RoomApiDeleteRequestData {
  userids: Array<string>;
}

interface RoomApiParticipantsRequestBody {
  type: RoomApiRequestType.PARTICIPANTS;
  participants: RoomApiParticipantsRequestData;
}

interface RoomApiParticipantsRequestData {
  changed: Array<Participant>;
  users: Array<Participant>;
}

interface RoomApiIncallRequestBody {
  type: RoomApiRequestType.INCALL;
  incall: RoomApiIncallRequestData;
}

interface RoomApiIncallRequestData {
  incall: number; // TODO Flags
  changed: Array<Participant>;
  users: Array<Participant>;
}

interface RoomApiMessageRequestBody {
  type: RoomApiRequestType.MESSAGE;
  message: RoomApiMessageRequestData;
}

interface RoomApiMessageRequestData {
  data: {
    chat: { refresh: true };
  }
}

export interface RoomApiRequestDataProperties {
  name: string;
  type: RoomType;
  'lobby-state': LobbyState;
  'lobby-timer': RoomApiDateObject|null;
  'read-only': ReadOnly;
  'active-since': RoomApiDateObject|null;
}

enum RoomApiRequestType {
  INVITE = 'invite',
  DISINVITE = 'disinvite',
  UPDATE = 'update',
  DELETE = 'delete',
  PARTICIPANTS = 'participants',
  INCALL = 'incall',
  MESSAGE = 'message'
}

enum RoomType {
  UNKNOWN_CALL = -1,
  ONE_TO_ONE_CALL = 1,
  GROUP_CALL,
  PUBLIC_CALL,
  CHANGELOG_CONVERSATION
}

enum LobbyState {
  NO = 0,
  YES
}

enum ReadOnly {
  READ_WRITE = 0,
  READ_ONLY
}

export interface Participant {
  inCall: number; // Combination of ParticipantFlags
  lastPing: number;
  sessionId: string;
  participantType: ParticipantType;
  userId: string | undefined; // for guests
}

enum ParticipantType {
  OWNER = 1,
  MODERATOR,
  USER,
  GUEST,
  USER_SELF_JOINED,
  GUEST_MODERATOR
}

enum ParticipantFlags {
  DISCONNECTED = 0,
  IN_CALL = 1,
  WITH_AUDIO = 2,
  WITH_VIDEO = 4
}

interface RoomApiDateObject {
  date: string,
  timezone: string,
  timezone_type: number
}

// }
