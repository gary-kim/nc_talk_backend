import WebSocket from 'ws';
import {RoomRequestData, BackendApiRequestHelper} from './BackendApiRequestHelper';

export class Session {

  private id: string;
  private socket: WebSocket;

  private ncUserId?: string;
  private ncRoomId?: string;
  private ncSessionId?: string;

  private eventHandlers: Map<string, Array<EventHandler>> = new Map();

  private subscriberIds: Map<string, any> = new Map();

  private backendApiRequestHelper: BackendApiRequestHelper;
  private webRtcServerHelper?: AbstractWebRtcServerHelper;

  constructor(options: ConstructorOptions, services: ConstructorServices) {
    this.id = options.sessionId;
    this.socket = options.socket;

    this.backendApiRequestHelper = services.backendApiRequestHelper;
    this.webRtcServerHelper = services.webRtcServerHelper;

    this.setupSocket();
  }

  destructor() {
    this.webRtcServerHelper?.unpublish(this.id);
    this.subscriberIds.forEach(id => {
      this.webRtcServerHelper?.unsubscribe(this.id, id);
    });
  }

  getId() {
    return this.id;
  }

  getUserId() {
    return this.ncUserId;
  }

  getRoomId() {
    return this.ncRoomId;
  }

  getNcSessionId() {
    return this.ncSessionId;
  }

  setUserId(userId: string) {
    this.ncUserId = userId;
  }

  private setupSocket() {
    this.socket.on('message', this.handleSocketMessage.bind(this));
    this.socket.on('close', this.handleSocketClosed.bind(this));
  }

  private handleSocketClosed() {
    this.fireEvent('socketClosed');
  }

  sendSocketMessage(data: Object) {
    this.socket.send(JSON.stringify(data));
  }

  private handleSocketMessage(message: string) {
    const data = JSON.parse(message);

    switch(data.type) {
      case MessageType.ROOM:
        this.handleRoomMessage(data.id, data.room);
        break;

      case MessageType.CONTROL:
        this.handleControlMessage(data.control);
        break;

      case MessageType.MESSAGE:
        this.handleMessageMessage(data.message);
        break;

      default:
        console.warn('message not understood!');
    }
  }

  private async handleRoomMessage(id: string, data: RoomMessageData) {
    console.log('ROOM:', data);

    const sessionId = data.sessionid || this.ncSessionId;
    if(!sessionId) throw new Error('session id not defined!');

    const roomId = data.roomid || this.ncRoomId;
    if(!roomId) throw new Error('room id not defined!');

    const action = data.roomid ? 'join' : 'leave';

    const sendData: RoomRequestData = {
      version: '1.0',
      userid: this.ncUserId,
      sessionid: sessionId,
      roomid: roomId,
      action: action
    };

    try {
      const backendResponse = await this.backendApiRequestHelper.sendRoomRequest(sendData);

      const response = {
        id: id,
        type: 'room',
        room: backendResponse
      };

      this.ncRoomId = data.roomid || undefined;
      this.ncSessionId = action === 'join' ? data.sessionid : undefined;

      this.sendSocketMessage(response);

      if(sendData.action === 'join') {
        this.webRtcServerHelper?.joinRoom(this.id, roomId);
        this.fireEvent('roomEvent', { roomId: sendData.roomid, type: RoomEventType.JOIN, eventData: [ { sessionid: this.id } ] }); // TODO session objects
      } else {
        this.webRtcServerHelper?.leaveRoom(this.id, roomId);
        this.fireEvent('roomEvent', { roomId: roomId, type: RoomEventType.LEAVE, eventData: [ this.id ] });
      }
    } catch(error) {
      console.error(error);
    }
  }

  private async handleControlMessage(data: ControlMessageData) {
    console.log('CONTROL:', data);
    await this.handleMessageMessage(data);
  }

  private async handleMessageMessage(data: MessageMessageData) {
    console.log('MESSAGE:', data);

    const recipient = data.recipient;

    switch(recipient.type) {
      case MessageMessageRecipientType.SESSION:
        // if(!senderSessionId) throw new Error('session of sender not found!');
        await this.handleMessageToSession(this.id, recipient, data.data);
        break;

      case MessageMessageRecipientType.USER:
        this.handleMessageToUser(recipient.userid, data.data);
        break;

      case MessageMessageRecipientType.ROOM:
        // if(!this.ncRoomId) throw new Error('session for socket not found!');
        this.handleMessageToRoom(this.ncRoomId, data.data);
        break;

      default:
        console.warn('Message not understood!', data);
    }
  }

  sendMessageMessageResponse(recipient: MessageRecipientData, response) {
    const messageResponse: MessageMessageResponse = {
      type: MessageType.MESSAGE,
      message: {
        sender: {
          type: MessageMessageRecipientType.SESSION,
          sessionid: recipient.sessionid,
          userid: recipient.userid
        },
        data: response
      }
    };

    this.sendSocketMessage(messageResponse);
  }

  private async handleMessageToSession(senderSessionId: string, recipient: MessageRecipientData, data) {
    this.fireEvent('sessionMessage', { recipient, senderSessionId, data });
  }

  private handleMessageToUser(userId: string, data) {
    this.fireEvent('userMessage', { userId, data });
  }

  private handleMessageToRoom(roomId: string, data) {
    this.fireEvent('roomMessage', { roomId, data });
  }

  on(eventName: string, eventHandler: EventHandler) {
    const eventHandlers = this.eventHandlers.get(eventName);
    if(!eventHandlers) {
      this.eventHandlers.set(eventName, [ eventHandler ]);
    } else {
      eventHandlers.push(eventHandler);
    }
  }

  fireEvent(eventName: string, eventData?) {
    const eventHandlers = this.eventHandlers.get(eventName);
    eventHandlers?.forEach(handler => {
      handler(eventData);
    });
  }

}

interface ConstructorOptions {
  sessionId: string;
  socket: WebSocket;
}

interface ConstructorServices {
  backendApiRequestHelper: BackendApiRequestHelper;
  webRtcServerHelper?: AbstractWebRtcServerHelper;
}

interface EventHandler {
  (data?): void;
}

interface MessageCommon {
  id: string
  type: MessageType
}

type Message = RoomMessage|ControlMessage|MessageMessage;

interface RoomMessage extends MessageCommon {
  type: MessageType.ROOM
  room: RoomMessageData
}

interface RoomMessageData {
  roomid?: string;
  // Pass the Nextcloud session id to the signaling server. The
  // session id will be passed through to Nextcloud to check if
  // the (Nextcloud) user is allowed to join the room.
  sessionid?: string;
}

interface ControlMessage extends MessageCommon {
  type: MessageType.CONTROL
  control: ControlMessageData
}

interface ControlMessageData {
  recipient: MessageRecipientData; // currently only to sessions?
  data;
}

interface MessageMessage extends MessageCommon {
  type: MessageType.MESSAGE
  message: MessageMessageData
}

interface MessageMessageData {
  recipient: MessageRecipientData
  data: {
    type: MessageMessageDataType,
    roomType: RoomType;
  };
}

export type MessageRecipientData = MessageSessionRecipientData | MessageRoomRecipientData | MessageUserRecipientData;

interface MessageSessionRecipientData {
  type: MessageMessageRecipientType.SESSION;
  sessionid: string;
}

interface MessageRoomRecipientData {
  type: MessageMessageRecipientType.ROOM
}

interface MessageUserRecipientData {
  type: MessageMessageRecipientType.USER
  userid: string;
}

enum MessageMessageDataType {
  REQUESTOFFER = 'requestoffer',
  SENDOFFER = 'sendoffer'
}

interface MessageMessageResponse {
  type: MessageType.MESSAGE;
  message: MessageMessageResponseData
}

interface MessageMessageResponseData {
  sender: MessageRecipientData;
  data;
}

enum MessageType {
  HELLO = 'hello',
  BYE = 'bye',
  ROOM = 'room',
  CONTROL = 'control',
  MESSAGE = 'message'
}

enum RoomEventType {
  JOIN = 'join',
  LEAVE = 'leave',
  CHANGE = 'change',
  MESSAGE = 'message'
}

export enum RoomListEventType {
  INVITE = 'invite',
  DISINVITE = 'disinvite',
  UPDATE = 'update'
}

enum MessageMessageRecipientType {
  SESSION = 'session',
  ROOM = 'room',
  USER = 'user'
}

enum RoomType {
  VIDEO = 'video',
  SCREEN = 'screen'
}
