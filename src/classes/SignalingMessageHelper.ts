import WebSocket from 'ws';

import {SessionManager} from './SessionManager';
import {BackendApiRequestHelper, AuthParams} from './BackendApiRequestHelper';
import {Participant} from './RoomApiHandler';

export class SignalingMessageHelper {

  private webRtcServerHelper?: AbstractWebRtcServerHelper;
  private sessionManager: SessionManager;
  private backendApiRequestHelper: BackendApiRequestHelper;

  constructor(webSocketServer: WebSocket.Server, sessionManager: SessionManager, backendApiRequestHelper: BackendApiRequestHelper, webRtcServerHelper?: AbstractWebRtcServerHelper) {
    this.sessionManager = sessionManager;
    this.backendApiRequestHelper = backendApiRequestHelper;
    this.webRtcServerHelper = webRtcServerHelper;

    webSocketServer.on('connection', this.onConnection.bind(this));
    webSocketServer.on('close', this.onDisconnect.bind(this));
  }

  private onConnection(socket: WebSocket) {
    console.log('user connected...');

    socket.on('message', (message: string) => {
      this.handleMessage(socket, message);
    });

    socket.on('close', () => {
      console.log('user disconnected (client).');
    });
  }

  private onDisconnect() {
    console.log('user disconnected (server).');
  }

  private handleMessage(socket: WebSocket, message: string) {
    const data: Message = JSON.parse(message);

    switch(data.type) {
      case MessageType.HELLO:
        this.handleHelloMessage(socket, data);
        break;

      case MessageType.BYE:
        this.handleByeMessage(socket, data);
        break;

      default:
        console.warn('message not understood!');
    }
  }

  private async handleHelloMessage(socket: WebSocket, data: HelloMessage) {
    console.log('HELLO:', data.hello);

    this.backendApiRequestHelper.setUrl(data.hello.auth.url);
    const backendResponse = await this.backendApiRequestHelper.sendAuthRequest({
      version: '1.0',
      ...data.hello.auth
    });

    const userId = backendResponse.userid;
    const session = await this.sessionManager.createSession({
      socket: socket,
      ncUserId: userId,
      backendApiRequestHelper: this.backendApiRequestHelper,
      webRtcServerHelper: this.webRtcServerHelper
    });

    const features = [];
    if(this.webRtcServerHelper) features.push('mcu');

    const response: HelloMessageResponse = {
      id: data.id,
      type: MessageType.HELLO,
      hello: {
        sessionid: session.id,
        resumeid: '',
        userid: userId,
        version: '1.0',
        server: {
          features: features
        }
      }
    };

    this.sendSocketMessage(socket, response);
  }

  private handleByeMessage(socket: WebSocket, data: ByeMessage) {
    console.log('BYE:', data.bye);

    const response: ByeMessageResponse = {
      id: data.id,
      type: MessageType.BYE,
      bye: {}
    };

    this.sendSocketMessage(socket, response);

    socket.close();
  }



  // ********** MESSAGE ANSWER **********

  private sendAnswerMessage(sender, data) {
    // this._sendSocketMessage(socket, {
    //   type: 'message',
    //   message: {
    //     sender: sender,
    //     data: data
    //   }
    // });
  }


  // ********** EVENTS **********

  private sendSocketMessage(socket: WebSocket, data: Object) {
    socket.send(JSON.stringify(data));
  }

}

interface MessageCommon {
  id: string
  type: MessageType
}

type Message = HelloMessage|ByeMessage;

interface HelloMessage extends MessageCommon {
  type: MessageType.HELLO
  hello: HelloMessageData
}

interface HelloMessageData {
  version: '1.0';
  auth: AuthMessageData;
}

interface AuthMessageData {
  url: string;
  params: AuthParams;
}

interface HelloMessageResponse {
  id: string
  type: MessageType.HELLO
  hello: HelloMessageResponseData
}

interface HelloMessageResponseData {
  sessionid: string
  resumeid: string
  userid?: string
  version: '1.0'
  server: {
    features: Array<string>
  }
}

interface ByeMessage extends MessageCommon {
  type: MessageType.BYE,
  bye: {}
}

interface ByeMessageResponse {
  id: string;
  type: MessageType.BYE;
  bye: {}
}

enum MessageType {
  HELLO = 'hello',
  BYE = 'bye',
  ROOM = 'room',
  CONTROL = 'control',
  MESSAGE = 'message'
}
