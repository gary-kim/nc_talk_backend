import * as randomstring from 'randomstring';
import * as crypto from 'crypto';
import Axios from 'axios';

import * as config from '../../config.json';

export class BackendApiRequestHelper {

  private url: string = '';

  setUrl(url: string) {
    this.url = url;
  }

  async sendAuthRequest(data: AuthRequestData): Promise<AuthResponseData> {
    const requestBody: AuthRequest = {
      type: BackendApiRequestType.AUTH,
      auth: {
        version: '1.0',
        params: data.params
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<AuthRequest, AuthResponse>(requestBody);

    return backendResponse.auth;
  }

  async sendRoomRequest(data: RoomRequestData): Promise<RoomResponseData> {
    const requestBody: RoomRequest = {
      type: BackendApiRequestType.ROOM,
      room: {
        version: '1.0',
        roomid: data.roomid,
        userid: data.userid,
        sessionid: data.sessionid,
        action: data.action
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<RoomRequest, RoomResponse>(requestBody);

    return backendResponse.room;
  }

  async sendPingRequest(data: PingRequestData): Promise<PingResponseData> {
    // TODO - find out what and why?
    const requestBody: PingRequest = {
      type: BackendApiRequestType.PING,
      ping: {
        roomid: data.roomid,
        entries: data.entries
      }
    };

    const backendResponse = await this.sendBackendAPIRequest<PingRequest, PingResponse>(requestBody);

    return backendResponse.room;
  }

  private async sendBackendAPIRequest<T, U>(data: T): Promise<U> {
    const randomString = randomstring.generate(32);

    const hmac = crypto.createHmac('sha256', process.env.SHARED_SECRET || config.sharedSecret);
    hmac.update(randomString);
    hmac.update(JSON.stringify(data));
    const checksum = hmac.digest('hex');

    const response = await Axios.post(this.url, data, {
      headers: {
        'Spreed-Signaling-Random': randomString,
        'Spreed-Signaling-Checksum': checksum,
        'OCS-APIRequest': true
      }
    });

    const responseData = response.data.ocs.data;

    return new Promise((res, rej) => {
      if(responseData.error) {
        rej(responseData.error);
      } else {
        res(responseData);
      }
    });
  }

}

enum BackendApiRequestType {
  AUTH = 'auth',
  ROOM = 'room',
  PING = 'ping'
}

interface AuthRequest {
  type: BackendApiRequestType.AUTH;
  auth: AuthRequestData;
}

export interface AuthRequestData {
  version: '1.0';
  params: AuthParams
}

export interface AuthParams {
  userid: string;
  ticket: string;
}

interface AuthResponse {
  type: BackendApiRequestType.AUTH;
  auth: AuthResponseData;
}

interface AuthResponseData {
  version: '1.0';
  userid?: string;
  user?: {
    displayname: string;
  };
}

interface RoomRequest {
  type: BackendApiRequestType.ROOM;
  room: RoomRequestData;
}

export interface RoomRequestData {
  version: '1.0';
  roomid: string;
  userid?: string;
  sessionid: string;
  action: 'join'|'leave';
}

interface RoomResponse {
  type: BackendApiRequestType.ROOM;
  room: RoomResponseData;
}

interface RoomResponseData {
  version: '1.0';
  roomid: string;
  properties: {}; // TODO
  permissions: [
    'publish-media',
    'publish-screen',
    'control'?
  ];
}

interface PingRequest {
  type: BackendApiRequestType.PING;
  ping: PingRequestData;
}

interface PingRequestData {
  roomid: string;
  entries: Array<PingRequestEntry>;
}

interface PingRequestEntry {
  sessionid: string;
  userid?: string;
}

interface PingResponse {
  type: BackendApiRequestType.ROOM;
  room: PingResponseData;
}

interface PingResponseData {
  version: '1.0';
  roomid: string;
}
