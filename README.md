# Nextcloud Talk Backend

Since the default internal signaling was not usable on my system and Nextcloud's own High-Performance-Backend is not affordable by private users, I decided to take a shot and implement a signaling server based on what can be found in the source of Nextcloud Talk and the API documentation.

## Signaling Server

The signaling server itself communicates over a WebSocket connection. For better performance it is recommended to use a separate MCU/SFU. I worked with Janus but I try to keep the interface abstract so it should be possible to expand the connectors for using other servers (Jitsi, etc.)
